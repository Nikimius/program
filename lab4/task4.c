//
//  task4.c
//  fourth_lab
//
//  Created by Eugene Gusev on 13.06.14.
//  Copyright (c) 2014 Eugene Gusev. All rights reserved.
//

#include <stdio.h>

int count=0, maxcount=0, maxnumber=0;

int odd (int number) {
    number=number*3+1;
    return number;
}

int even (int number) {
    number=number/2;
    return number;
}

int getKoll (int number) {
    if (number!=1) {
        if (number%2==0) {
            count++;
            getKoll(even(number));
          }
        if (number%2==1) {
            count++;
            getKoll(odd(number));
          }
    }
    if (count>maxcount) {
        maxcount=count;
    }
    return count;
}

int main () {
    int number=2, backup=0;
    for (; number<=1000000; number++) {
        backup=number;
        if (getKoll(number)==maxcount) {
            maxnumber=backup;
        }
        count=0;
    }
    printf("Max Collatz chain was recieved by number %d with %d cycles",maxnumber,maxcount);
    return 0;
}
// P.S. получилось число 910107 с 475 элементами в последовательности